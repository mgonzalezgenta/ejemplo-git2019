CXX = gcc
CFLAGS = 
LIBS = 
LDFLAGS = 

BUILD_DIR= bin
OUTPUT_FILES_DIR= build

INCLUDE= include/
INC_PATHS = $(foreach d, $(INCLUDE), -I$d)

TARGET_TEST = ./test/

OBJS_SRC = $(patsubst %.c,%.o,$(wildcard ./src/****/*.c))
OBJS_SRC+= $(patsubst %.c,%.o,$(wildcard ./src/*.c))
OBJS_TEST = $(patsubst %.c,%.o,$(wildcard ./test/**.c))

$(info $(OBJS_SRC))

all: test

test:  $(OBJS_SRC) $(OBJS_TEST)
	$(info LINKEANDO $(patsubst %.o, $(OUTPUT_FILES_DIR)/%.o,$(filter %.o, $(subst /, , $^))) para $@...)
	$(CXX) $(LDFLAGS) -o $(BUILD_DIR)/$(lastword $(subst /, , $@)) $(patsubst %.o, $(OUTPUT_FILES_DIR)/%.o,$(filter %.o, $(subst /, , $^))) $(LIBS) $(INC_PATHS)

%.o:%.c
	$(info COMPILANDO $<...)
	$(CXX) $(CFLAGS) $< -o $(OUTPUT_FILES_DIR)/$(lastword $(subst /, , $@)) $(INC_PATHS) -c

.PHONY: clean
clean:
	find . -type f -name '*.o' -delete
	rm -f ./bin/*


